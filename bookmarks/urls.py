from django.urls import path
from . import views


urlpatterns = [
    path('bookmarked_contracts', views.BookmarkedContractListCreate.as_view(), name='bookmarked-contracts'),
    path('bookmarked_tenders', views.BookmarkedTenderListCreate.as_view(), name='bookmarked-tenders'),

    path('bookmarked_contracts/<int:pk>', views.SingleBookmarkedContract.as_view(), name='single-bookmarked-contract'),
    path('bookmarked_tenders/<int:pk>', views.SingleBookmarkedTender.as_view(), name='single-bookmarked-tender'),

]
