from rest_framework import serializers

from .models import BookmarkedTender, BookmarkedContract


class BookmarkedContractSerializer(serializers.ModelSerializer):
    # user = HuplerUserSerializer()
    # contract = ContractSerializer()

    class Meta:
        model = BookmarkedContract
        fields = ['id', 'contract', 'user', 'is_active']


class BookmarkedTenderSerializer(serializers.ModelSerializer):
    # user = HuplerUserSerializer()
    # tender = TenderSerializer()

    class Meta:
        model = BookmarkedTender
        fields = ['id', 'tender', 'user', 'is_active']
