from rest_framework import generics

from .models import BookmarkedTender, BookmarkedContract
from .serializers import BookmarkedContractSerializer, BookmarkedTenderSerializer

# Create your views here.


class BookmarkedContractListCreate(generics.ListCreateAPIView):
    queryset = BookmarkedContract.objects.all()
    serializer_class = BookmarkedContractSerializer


class BookmarkedTenderListCreate(generics.ListCreateAPIView):
    queryset = BookmarkedTender.objects.all()
    serializer_class = BookmarkedTenderSerializer


class SingleBookmarkedContract(generics.RetrieveUpdateAPIView):
    queryset = BookmarkedContract.objects.all()
    serializer_class = BookmarkedContractSerializer


class SingleBookmarkedTender(generics.RetrieveUpdateAPIView):
    queryset = BookmarkedTender.objects.all()
    serializer_class = BookmarkedTenderSerializer
