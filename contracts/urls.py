from django.urls import path
from . import views

urlpatterns = [
    path('contracts/', views.ContractList.as_view(), name='contracts-list'),
    path('contract_locations/', views.LocationList.as_view(), name='contracts_locations'),
]
