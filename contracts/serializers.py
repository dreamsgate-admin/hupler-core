from rest_framework import serializers
from .models import Category, Company, Contract, JobDomain, Location, Skill


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'category_name']


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ['id', 'location_name']


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = ['id', 'skill_name']


class JobDomainSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobDomain
        fields = ['id', 'domain_name']


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'company_name', 'company_logo_url']


class ContractSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    company = CompanySerializer()
    location = LocationSerializer()
    skills = SkillSerializer(many=True)
    domains = JobDomainSerializer(many=True)

    class Meta:
        model = Contract
        fields = [
            'id',
            'title',
            'posted_datetime',
            'salary',
            'description',
            'url',
            'category',
            'company',
            'location',
            'skills',
            'domains',
        ]

