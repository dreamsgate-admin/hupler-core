from django.db import models

# Create your models here.


class Category(models.Model):

    category_name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.category_name.title()


class Location(models.Model):

    location_name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.location_name.title()


class Skill(models.Model):

    skill_name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.skill_name.title()


class JobDomain(models.Model):

    domain_name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.domain_name.title()


class Company(models.Model):

    company_name = models.CharField(max_length=200, unique=True)
    company_logo_url = models.URLField()

    def __str__(self):
        return self.company_name.title()


class Contract(models.Model):

    title = models.TextField(default='Title not available')
    posted_datetime = models.DateTimeField(null=True, blank=True)
    salary = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    url = models.URLField(unique=True)

    category = models.ForeignKey(Category, related_name='contracts', on_delete=models.SET_NULL, null=True, blank=True)
    company = models.ForeignKey(Company, related_name='contracts', on_delete=models.SET_NULL, null=True, blank=True)
    location = models.ForeignKey(Location, related_name='contracts', on_delete=models.SET_NULL, null=True, blank=True)

    skills = models.ManyToManyField(Skill, related_name='contracts', blank=True)
    domains = models.ManyToManyField(JobDomain, related_name='contracts', blank=True)

    def __str__(self):
        return f"{self.title}"
