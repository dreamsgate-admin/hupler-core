from django.core.paginator import Paginator

from rest_framework import generics
import rest_framework_filters as filters
from rest_framework.pagination import PageNumberPagination

from .models import Contract, Location
from .serializers import ContractSerializer, LocationSerializer
# Create your views here.

# from https://github.com/carltongibson/django-filter/issues/137


class ContractsPagination(PageNumberPagination):
    django_paginator_class = Paginator
    page_size = 2


class ContractListFilterSet(filters.FilterSet):
    location = filters.AllValuesMultipleFilter()

    class Meta:
        model = Contract
        fields = ['location']


class ContractList(generics.ListAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    filterset_class = ContractListFilterSet
    pagination_class = ContractsPagination


class LocationList(generics.ListAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

