from django.apps import AppConfig


class SubscriptionplansConfig(AppConfig):
    name = 'subscriptionplans'
