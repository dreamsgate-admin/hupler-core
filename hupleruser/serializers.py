from django_countries.serializers import CountryFieldMixin
from rest_framework import serializers

from .models import HuplerUser


class HuplerUserSerializer(CountryFieldMixin, serializers.ModelSerializer):

    class Meta:
        model = HuplerUser
        fields = '__all__'
