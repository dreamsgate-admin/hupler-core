from django.db import models
from django.contrib.auth.models import AbstractUser
from django_countries.fields import CountryField

# Create your models here.


class HuplerUser(AbstractUser):
    country = CountryField()
    email = models.EmailField(unique=True)
