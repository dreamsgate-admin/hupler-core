from django.core.paginator import Paginator

from rest_framework import generics
import rest_framework_filters as filters
from rest_framework.pagination import PageNumberPagination


from .models import Tender, TenderSource, Location
from .serializers import TenderSerializer, LocationSerializer


class TenderPagination(PageNumberPagination):
    django_paginator_class = Paginator
    page_size = 2


class TenderListFilterSet(filters.FilterSet):
    location = filters.AllValuesMultipleFilter()
    source = filters.ModelChoiceFilter(queryset=TenderSource.objects.all())

    class Meta:
        model = Tender
        fields = ['location', 'source']


class TenderList(generics.ListAPIView):
    queryset = Tender.objects.all()
    serializer_class = TenderSerializer
    filterset_class = TenderListFilterSet
    pagination_class = TenderPagination


class LocationList(generics.ListAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
