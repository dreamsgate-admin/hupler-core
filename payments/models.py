from django.db import models

from hupleruser.models import HuplerUser
# Create your models here.


class Transaction(models.Model):

    user = models.ForeignKey(HuplerUser, related_name='transactions', on_delete=models.CASCADE)
    transaction_id = models.CharField(max_length=50, unique=True)
    transaction_type = models.CharField(max_length=50)
    amount = models.FloatField()
    status = models.CharField(max_length=50)  # Can be a FK to a model. There are finite choices for this
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    def __str__(self):
        return f"{self.transaction_id}"


class PaymentDetail(models.Model):

    user = models.ForeignKey(HuplerUser, related_name='payment_details', on_delete=models.CASCADE)
    token = models.CharField(max_length=100, null=True, blank=True)  # Can be some other field? Need to check in the future
    bank_identification_number = models.IntegerField()
    last_4_digits = models.IntegerField()
    card_type = models.CharField(max_length=100)
    expiration_month = models.IntegerField()
    expiration_year = models.IntegerField()
    cardholder_name = models.CharField(max_length=200, null=True, blank=True)
    customer_location = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.last_4_digits} | {self.card_type}"
