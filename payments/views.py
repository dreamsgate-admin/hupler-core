from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.urls import reverse
from django.contrib import messages
from django.shortcuts import redirect

from .utils import generate_client_token, find_transaction, transact
from .models import PaymentDetail, Transaction
# Create your views here.


@login_required
def new_checkout(request):
    client_token = generate_client_token()
    if request.method == 'GET':
        context = {
            'client_token': client_token,
        }
        return render(request, 'payments/new.html', context=context)

    if request.method == 'POST':
        result = transact({
            'amount': request.POST.get('amount'),
            'payment_method_nonce': request.POST.get('payment_method_nonce'),
            'options': {
                "submit_for_settlement": True
            }
        })

        if result.is_success or result.transaction:
            return redirect(reverse('show-checkout',
                            kwargs={
                                'transaction_id': result.transaction.id
                            }
            ))

        else:
            for x in result.errors.deep_errors:
                messages.info(request, x)
            return redirect(reverse('new-checkout'))


@login_required
def show_checkout(request, transaction_id):

    hupler_user = request.user
    transaction = find_transaction(transaction_id)
    if transaction.status in settings.TRANSACTION_SUCCESS_STATUSES:
        context = {
            'header': 'Sweet Success!',
            'icon': 'success',
            'message': 'Your test transaction has been successfully processed. See the Braintree API response and try again.',
            'transaction': transaction,
        }

    else:
        context = {
            'header': 'Transaction Failed',
            'icon': 'fail',
            'message': 'Your test transaction has a status of ' + transaction.status + '. See the Braintree API response and try again.',
            'transaction': transaction,
        }
    credit_card = transaction.credit_card_details

    payment_detail = PaymentDetail.objects.create(
        user=hupler_user,
        token=credit_card.token,
        bank_identification_number=credit_card.bin,
        last_4_digits=credit_card.last_4,
        card_type=credit_card.card_type,
        expiration_year=transaction.credit_card['expiration_year'],
        expiration_month=transaction.credit_card['expiration_month'],
        cardholder_name=credit_card.cardholder_name,
        customer_location=credit_card.customer_location,
    )
    payment_detail.save()

    transaction_object = Transaction.objects.create(
        user=hupler_user,
        transaction_id=transaction.id,
        transaction_type=transaction.type,
        amount=transaction.amount,
        status=transaction.status,
        created_at=transaction.created_at,
        updated_at=transaction.updated_at,
    )
    transaction_object.save()

    return render(request, 'payments/show.html', context=context)
