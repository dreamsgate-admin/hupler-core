from django.urls import path
from . import views


urlpatterns = [
    path('new/', views.new_checkout, name='new-checkout'),
    path('<transaction_id>/', views.show_checkout, name='show-checkout'),
    # path('create/', views.create_checkout, name='create-checkout'),
]
